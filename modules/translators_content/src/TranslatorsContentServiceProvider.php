<?php

namespace Drupal\translators_content;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\translators_content\Access\TranslatorsContentManageAccessCheck;
use Drupal\translators_content\Access\TranslatorsContentOverviewAccess;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class TranslatorsContentServiceProvider.
 *
 * @package Drupal\translators_content
 */
class TranslatorsContentServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Replace access manager services classes with our own.
    static $access_services = [
      'content_translation.manage_access',
      'content_translation.overview_access',
    ];
    $arguments = [
      new Reference('entity_type.manager'),
      new Reference('language_manager'),
      new Reference('current_user'),
      new Reference('translators.skills'),
    ];
    $container->getDefinition('content_translation.manage_access')
      ->setClass(TranslatorsContentManageAccessCheck::class)
      ->setArguments($arguments);
    $container->getDefinition('content_translation.overview_access')
      ->setClass(TranslatorsContentOverviewAccess::class);
  }

}
