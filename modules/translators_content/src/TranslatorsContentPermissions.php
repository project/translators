<?php

namespace Drupal\translators_content;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\content_translation\ContentTranslationPermissions;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TranslatorsContentPermissions.
 *
 * @package Drupal\translators_content
 */
class TranslatorsContentPermissions extends ContentTranslationPermissions {

  /**
   * Module name.
   */
  const MODULE_NAME = 'translators_content';
  /**
   * Permissions suffix.
   */
  const PERMISSIONS_SUFFIX = '(in translation skills)';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('content_translation.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function contentPermissions() {
    $permissions = [];

    $this->addStaticPermissions($permissions);

    // Create a translate permission for each enabled entity type
    // and (optionally) bundle.
    $definitions = $this->entityTypeManager->getDefinitions();
    foreach ($definitions as $entity_type_id => $entity_type) {
      if ($permission_granularity = $entity_type->getPermissionGranularity()) {
        if ($permission_granularity === 'bundle'
          && ($entity_type_id == 'node'
          || $entity_type_id == 'taxonomy_term'
          || $entity_type_id == 'media')) {
          $bundles_info = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
          foreach ($bundles_info as $bundle => $bundle_info) {
            $this->addBundleGlobalPermissions($permissions, $entity_type_id, $entity_type, $bundle, $bundle_info['label']);
          }
        }
      }
    }

    return $permissions;
  }

  /**
   * Add static permissions.
   *
   * @param array &$permissions
   *   Permissions array to be used for putting in.
   */
  protected function addStaticPermissions(array &$permissions) {
    $t = [
      '@suffix' => static::PERMISSIONS_SUFFIX,
    ];
    $permissions[static::MODULE_NAME . ' create content translations']
      = $this->t('Create translations @suffix', $t);
    $permissions[static::MODULE_NAME . ' update content translations']
      = $this->t('Edit translations @suffix', $t);
    $permissions[static::MODULE_NAME . ' delete content translations']
      = $this->t('Delete translations @suffix', $t);
  }

  /**
   * Add bundle global permissions.
   *
   * @param array &$permissions
   *   Permissions array.
   * @param string $entity_type_id
   *   Entity type ID.
   * @param ContentEntityType $entity_type
   *   Entity type.
   * @param string $bundle_id
   *   Entity bundle ID.
   * @param string $bundle_label
   *   Entity bundle label.
   */
  protected function addBundleGlobalPermissions(array &$permissions, string $entity_type_id, ContentEntityType $entity_type, $bundle_id, $bundle_label = NULL) {
    // Fallback for the cases if bundle has no label,
    // in this case we gonna use it's ID(machine name).
    if (empty($bundle_label)) {
      $bundle_label = $bundle_id;
    }
    $entity_label = strtolower($entity_type->getLabel());
    // Prepare translation arguments array.
    $t = [
      '@entity_label' => $entity_label,
      '%bundle_label' => $bundle_label,
      '@suffix'       => static::PERMISSIONS_SUFFIX,
    ];
    // Add "global" permissions for the basic actions on the entity bundles.
    $permissions[static::MODULE_NAME . " create $bundle_id $entity_type_id"] = [
      'title' => $this->t('%bundle_label: Create new @entity_label @suffix', $t),
    ];
    $permissions[static::MODULE_NAME . " edit any $bundle_id $entity_type_id"] = [
      'title' => $this->t('%bundle_label: Edit @entity_label @suffix', $t),
    ];
    $permissions[static::MODULE_NAME . " delete any $bundle_id $entity_type_id"] = [
      'title' => $this->t('%bundle_label: Delete @entity_label @suffix', $t),
    ];
  }

}
