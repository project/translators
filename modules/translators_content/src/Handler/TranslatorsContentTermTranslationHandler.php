<?php

namespace Drupal\translators_content\Handler;

use Drupal\taxonomy\TermTranslationHandler;

/**
 * Class TranslatorsContentTermTranslationHandler.
 *
 * @package Drupal\translators_content
 */
class TranslatorsContentTermTranslationHandler extends TermTranslationHandler {
  use TranslatorsContentTranslationHandlerTrait;

}
