<?php

namespace Drupal\translators_content\Handler;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Class TranslatorsContentTranslationHandler.
 *
 * @package Drupal\translators_content
 */
class TranslatorsContentTranslationHandler extends ContentTranslationHandler {
  use TranslatorsContentTranslationHandlerTrait;

}
