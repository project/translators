<?php

namespace Drupal\translators_content\Handler;

use Drupal\comment\CommentTranslationHandler;

/**
 * Class TranslatorsContentCommentTranslationHandler.
 *
 * @package Drupal\translators_content
 */
class TranslatorsContentCommentTranslationHandler extends CommentTranslationHandler {
  use TranslatorsContentTranslationHandlerTrait;

}
