<?php

namespace Drupal\translators_content\Handler;

use Drupal\block_content\BlockContentTranslationHandler;

/**
 * Class TranslatorsContentBlockContentTranslationHandler.
 *
 * @package Drupal\translators_content
 */
class TranslatorsContentBlockContentTranslationHandler extends BlockContentTranslationHandler {
  use TranslatorsContentTranslationHandlerTrait;

}
