<?php

namespace Drupal\translators_content\Handler;

use Drupal\user\ProfileTranslationHandler;

/**
 * Class TranslatorsContentProfileTranslationHandler.
 *
 * @package Drupal\translators_content
 */
class TranslatorsContentProfileTranslationHandler extends ProfileTranslationHandler {
  use TranslatorsContentTranslationHandlerTrait;

}
