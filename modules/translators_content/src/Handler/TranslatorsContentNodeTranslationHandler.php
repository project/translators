<?php

namespace Drupal\translators_content\Handler;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\NodeTranslationHandler;
use Drupal\node\Entity\Node;

/**
 * Class TranslatorsContentContentTranslationHandler.
 *
 * @package Drupal\translators_content
 */
class TranslatorsContentNodeTranslationHandler extends NodeTranslationHandler {
  use TranslatorsContentTranslationHandlerTrait {
    getTranslationAccess as traitGetTranslationAccess;
    getOperationTranslationAccess as traitGetOperationTranslationAccess;
    getBundleTranslationAccess as traitGetBundleTranslationAccess;
    getCreateTranslationAccess as traitGetCreateTranslationAccess;
    hasOperationTranslationAccess as traitHasOperationTranslationAccess;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslationAccess(EntityInterface $entity, $operation, $active = NULL, $target = NULL) {
    $accessResult = $this->traitGetTranslationAccess($entity, $operation, $active, $target);
    if ($accessResult->isAllowed()) {
      return $accessResult;
    }
    elseif (($entity instanceof Node)
      && ($this->currentUser->hasPermission('bypass node access'))) {
      return AccessResult::allowed()
        ->cachePerPermissions()->addCacheableDependency($entity);
    }
    return $accessResult;
  }

  /**
   * {@inheritdoc}
   */
  public function getOperationTranslationAccess($entity, $operation, $langcode) {
    $accessResult = $this->traitGetOperationTranslationAccess($entity, $operation, $langcode);
    if (!$accessResult->isNeutral()) {
      return $accessResult;
    }
    elseif (($entity instanceof Node)
      && ($this->currentUser->hasPermission('bypass node access'))) {
      return AccessResult::allowed()
        ->cachePerPermissions()->addCacheableDependency($entity);
    }
    return $accessResult;
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleTranslationAccess(EntityInterface $entity) {
    $accessResult = $this->traitGetBundleTranslationAccess($entity);
    if (!$accessResult->isNeutral()) {
      return $accessResult;
    }
    elseif (($entity instanceof Node)
      && ($this->currentUser->hasPermission('bypass node access'))) {
      return AccessResult::allowed()
        ->cachePerPermissions()->addCacheableDependency($entity);
    }
    return $accessResult;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreateTranslationAccess($entity, $source, $target) {
    $accessResult = $this->traitGetCreateTranslationAccess($entity, $source, $target);
    if (!$accessResult->isNeutral()) {
      return $accessResult;
    }
    elseif (($entity instanceof Node)
      && ($this->currentUser->hasPermission('bypass node access'))) {
      return AccessResult::allowed()
        ->cachePerPermissions()->addCacheableDependency($entity);
    }
    return $accessResult;
  }

  /**
   * {@inheritdoc}
   */
  public function hasOperationTranslationAccess() {
    $result = $this->traitHasOperationTranslationAccess();
    if ($result == TRUE) {
      return $result;
    }
    elseif ($this->currentUser->hasPermission('bypass node access')) {
      return TRUE;
    }
    return FALSE;
  }

}
