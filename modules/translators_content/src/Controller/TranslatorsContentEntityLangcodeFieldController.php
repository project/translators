<?php

namespace Drupal\translators_content\Controller;

use Drupal\Core\Language\Language;

/**
 * Class TranslatorsContentEntityLangcodeFieldController.
 *
 * @package Drupal\translators_content\Controller
 */
class TranslatorsContentEntityLangcodeFieldController {

  /**
   * List of langcodes that shouldn't be filtered.
   *
   * @var array
   */
  protected static $ignoredLangcodes = [
    Language::LANGCODE_NOT_APPLICABLE,
    Language::LANGCODE_NOT_SPECIFIED,
  ];

  /**
   * After build callback for the langcode element on node forms.
   *
   * @param array $element
   *   Element render-able array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Updated element.
   */
  public static function entityFormLangcodeAfterBuild(array $element, &$form_state) {
    static::languageOptionFilteringController($element[0]['value']['#options'], $form_state);
    return $element;
  }

  /**
   * Filter languages list.
   *
   * @param array &$options
   *   Dropdown options array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected static function languageOptionFilteringController(array &$options, &$form_state) {
    $user = \Drupal::currentUser();
    $build_info = $form_state->getBuildInfo();
    $entity_form = $build_info['callback_object'];
    $entity = $entity_form->getEntity();
    $entity_type_id = $entity->getEntityTypeId();
    $bundle_id = $entity->bundle();
    $config = \Drupal::configFactory()->get('translators.settings');
    $default_permissions = ["create $bundle_id $entity_type_id"];
    $class = get_class($entity);
    switch ($class) {
      case 'Drupal\node\Entity\Node':
        $default_permissions = [
          "create $bundle_id content",
          'bypass node access',
          'administer nodes',
        ];
        break;

      case 'Drupal\media\Entity\Media':
        $default_permissions = ["create $bundle_id media", "create media"];
        break;

      case 'Drupal\taxonomy\Entity\Term':
        $default_permissions = ["create terms in $bundle_id"];
        break;

    }

    foreach ($default_permissions as $permission) {
      if ($user->hasPermission($permission)) {
        return;
      }
    }
    if ($user->hasPermission("translators_content create $bundle_id $entity_type_id")) {
      if ($config->get('enable_strict_translation_skill_pairing') == TRUE) {
        // Remove if user doesn't have the source-target language pair.
        static::filterLanguageOptions($options, 'source_skill_languages_only');
      }
      else {
        // Remove if user doesn't have the source language.
        static::filterLanguageOptions($options, 'all_skill_languages');
      }
    }

  }

  /**
   * Filter languages options.
   *
   * @param array &$options
   *   Dropdown options array.
   * @param string $mode
   *   Language option filtering mode. Possible values are:
   *   - "source_skill_languages_only"
   *   - "all_skill_languages"
   */
  public static function filterLanguageOptions(array &$options, string $mode) {
    $translationSkills = \Drupal::service('translators.skills');
    if ($mode == 'source_skill_languages_only') {
      foreach ($options as $langcode => $label) {
        if (in_array($langcode, static::$ignoredLangcodes)) {
          continue;
        }
        if (!in_array($langcode, $translationSkills->getSourceLangcodes())) {
          unset($options[$langcode]);
        }
      }
    }
    elseif ($mode == 'all_skill_languages') {
      foreach ($options as $langcode => $label) {
        if (in_array($langcode, static::$ignoredLangcodes)) {
          continue;
        }
        if (!$translationSkills->hasLangcode($langcode)) {
          unset($options[$langcode]);
        }
      }
    }
  }

}
