<?php

namespace Drupal\translators_content\Access;

use Drupal\content_translation\Access\ContentTranslationOverviewAccess;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Class TranslatorsContentOverviewAccess.
 *
 * @package Drupal\translators_content\Access
 */
class TranslatorsContentOverviewAccess extends ContentTranslationOverviewAccess {

  /**
   * {@inheritdoc}
   */
  public function access(RouteMatchInterface $route_match, AccountInterface $account, $entity_type_id) {
    /* @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $route_match->getParameter($entity_type_id);
    if ($entity && $entity->isTranslatable()) {
      $handler = $this->entityTypeManager->getHandler($entity->getEntityTypeId(), 'translation');
      if ($handler->getBundleTranslationAccess($entity)->isAllowed()
        && $handler->hasOperationTranslationAccess()) {
        return AccessResult::allowed()->cachePerPermissions()->addCacheableDependency($entity);
      }
    }
    // No opinion.
    return AccessResult::neutral();
  }

}
