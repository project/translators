<?php

namespace Drupal\translators_content\Access;

use Drupal\content_translation\Access\ContentTranslationManageAccessCheck;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\translators\Services\TranslatorSkills;
use Symfony\Component\Routing\Route;

/**
 * Class TranslatorsContentManageAccessCheck.
 *
 * @package Drupal\translators_content\Access
 */
class TranslatorsContentManageAccessCheck extends ContentTranslationManageAccessCheck {

  /**
   * Current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;
  /**
   * Translator skills service.
   *
   * @var \Drupal\translators\Services\TranslatorSkills
   */
  protected $translatorSkills;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityTypeManager $entity_type_manager,
    LanguageManagerInterface $language_manager,
    AccountInterface $currentUser,
    TranslatorSkills $translatorSkills
  ) {
    parent::__construct($entity_type_manager, $language_manager);
    $this->currentUser      = $currentUser;
    $this->translatorSkills = $translatorSkills;
  }

  /**
   * {@inheritdoc}
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account, $source = NULL, $target = NULL, $language = NULL, $entity_type_id = NULL) {
    /* @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    if ($entity = $route_match->getParameter($entity_type_id)) {
      $operation = $route->getRequirement('_access_content_translation_manage');
      $language = $this->languageManager->getLanguage($language) ?: $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT);
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);

      if (in_array($operation, ['update', 'delete'])) {
        // Translation operations cannot be performed on the default
        // translation.
        if ($language->getId() == $entity->getUntranslated()->language()->getId()) {
          return AccessResult::forbidden()->addCacheableDependency($entity);
        }
        // Editors have no access to the translation operations, as entity
        // access already grants them an equal or greater access level.
        $templates = ['update' => 'edit-form', 'delete' => 'delete-form'];
        if ($entity->access($operation) && $entity_type->hasLinkTemplate($templates[$operation])) {
          return AccessResult::forbidden()->cachePerPermissions();
        }
      }

      switch ($operation) {
        case 'create':
          /* @var \Drupal\content_translation\ContentTranslationHandlerInterface $handler */
          $handler = $this->entityTypeManager->getHandler($entity->getEntityTypeId(), 'translation');
          $translations = $entity->getTranslationLanguages();
          $languages = $this->languageManager->getLanguages();
          $source_language = $this->languageManager->getLanguage($source) ?: $entity->language();
          $target_language = $this->languageManager->getLanguage($target) ?: $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT);
          $is_new_translation = ($source_language->getId() != $target_language->getId()
            && isset($languages[$source_language->getId()])
            && isset($languages[$target_language->getId()])
            && !isset($translations[$target_language->getId()]));
          return AccessResult::allowedIf($is_new_translation)->cachePerPermissions()->addCacheableDependency($entity)
            ->andIf($handler->getTranslationAccess($entity, 'create', $source, $target));

        case 'delete':
          // @todo Remove this in https://www.drupal.org/node/2945956.
          /** @var \Drupal\Core\Access\AccessResultInterface $delete_access */
          $delete_access = \Drupal::service('content_translation.delete_access')->checkAccess($entity);
          $access = $this->checkAccess($entity, $language, $operation);
          return $delete_access->andIf($access);

        case 'update':
          return $this->checkAccess($entity, $language, $operation);
      }
    }

    // No opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(ContentEntityInterface $entity, LanguageInterface $language, $operation) {
    /* @var \Drupal\content_translation\ContentTranslationHandlerInterface $handler */
    $handler = $this->entityTypeManager->getHandler($entity->getEntityTypeId(), 'translation');
    $translations = $entity->getTranslationLanguages();
    $languages = $this->languageManager->getLanguages();
    $has_translation = isset($languages[$language->getId()])
      && $language->getId() != $entity->getUntranslated()->language()->getId()
      && isset($translations[$language->getId()]);
    return AccessResult::allowedIf($has_translation)->cachePerPermissions()->addCacheableDependency($entity)
      ->andIf($handler->getTranslationAccess($entity, $operation, $language->getId()));
  }

}
