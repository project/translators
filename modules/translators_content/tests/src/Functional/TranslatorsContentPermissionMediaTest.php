<?php

namespace Drupal\Tests\translators_content\Functional;

use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;

/**
 * Class TranslatorsContentPermissionNodeTest.
 *
 * @package Drupal\Tests\translators_content\Functional
 *
 * @group translators_content
 */
class TranslatorsContentPermissionMediaTest extends TranslatorsContentPermissionTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['translators_content', 'media'];
  /**
   * {@inheritdoc}
   */
  protected $entityTypeId = 'media';
  /**
   * {@inheritdoc}
   */
  protected $bundle = 'image';
  /**
   * {@inheritdoc}
   */
  protected $entityPermissionSupport = TRUE;
  /**
   * {@inheritdoc}
   */
  protected $file;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('/admin/config/media/media-settings');
    $this->submitForm(['standalone_url' => TRUE], 'Save configuration');
    $this->drupalLogout();
    file_put_contents('public://image-test.jpg', file_get_contents(__DIR__ . '/image-test.jpg'));
    $file = File::create([
      'uid' => 1,
      'filename' => 'image-test.jpg',
      'uri' => 'public://image-test.jpg',
      'filemime' => 'image/jpeg',
      'status' => 1,
    ]);
    $file->save();
    $this->file = $file;
  }

  /**
   * {@inheritdoc}
   */
  public function testTranslatonPermissionsVisability() {
    parent::testTranslatonPermissionsVisability();
    $this->runTestEntityPermissionsVisability();
  }

  /**
   * Test create entity any language permissions.
   */
  public function testCreateEntityAnyLanguage() {
    $bundle = $this->bundle;
    $user = $this->createUser([
      "create $bundle media",
    ]);
    $defaultStatus = $this->getDefaultExpectedStatus(403);
    $expectedStatus = [
      'create entity in any language' => 200,
      'create entity in translation skills' => 200,
    ];
    $this->expectedStatus = array_merge($defaultStatus, $expectedStatus);
    $this->runTests($user);
  }

  /**
   * Test edit entity any language permissions.
   */
  public function testEditAnyEntityAnyLanguage() {
    $bundle = $this->bundle;
    $user = $this->createUser([
      "edit any $bundle media",
    ]);
    $defaultStatus = $this->getDefaultExpectedStatus(403);
    $expectedStatus = [
      'edit any entity in any language' => 200,
      'edit any entity in translation skills' => 200,
      'edit own entity' => 200,
    ];
    $this->expectedStatus = array_merge($defaultStatus, $expectedStatus);
    $this->runTests($user);
  }

  /**
   * Test edit own entity permissions.
   */
  public function testEditOwnEntity() {
    $bundle = $this->bundle;
    $user = $this->createUser([
      "edit own $bundle media",
    ]);
    $defaultStatus = $this->getDefaultExpectedStatus(403);
    $expectedStatus = [
      'edit own entity' => 200,
    ];
    $this->expectedStatus = array_merge($defaultStatus, $expectedStatus);
    $this->runTests($user);
  }

  /**
   * Test delete entity any language permissions.
   */
  public function testDeleteAnyEntityAnyLanguage() {
    $bundle = $this->bundle;
    $user = $this->createUser([
      "delete any $bundle media",
    ]);
    $defaultStatus = $this->getDefaultExpectedStatus(403);
    $expectedStatus = [
      'delete any entity in any language' => 200,
      'delete any entity in translation skills' => 200,
      'delete own entity' => 200,
    ];
    $this->expectedStatus = array_merge($defaultStatus, $expectedStatus);
    $this->runTests($user);
  }

  /**
   * Test delete own entity permissions.
   */
  public function testDeleteOwnEntity() {
    $bundle = $this->bundle;
    $user = $this->createUser([
      "delete own $bundle media",
    ]);
    $defaultStatus = $this->getDefaultExpectedStatus(403);
    $expectedStatus = [
      'delete own entity' => 200,
    ];
    $this->expectedStatus = array_merge($defaultStatus, $expectedStatus);
    $this->runTests($user);
  }

  /**
   * Test create entity in translation skills permissions.
   */
  public function testCreateEntityInTranslationSkills() {
    $bundle = $this->bundle;
    $user = $this->createUser([
      "translators_content create $bundle media",
    ]);
    $default_status = $this->getDefaultExpectedStatus(403);
    $expectedStatus = [
      'create entity in translation skills' => 200,
    ];
    $this->expectedStatus = array_merge($default_status, $expectedStatus);
    $this->runTests($user);
  }

  /**
   * Test edit entity in translation skills permissions.
   */
  public function testEditEntityInTranslationSkills() {
    $bundle = $this->bundle;
    $user = $this->createUser([
      "translators_content edit any $bundle media",
    ]);
    $default_status = $this->getDefaultExpectedStatus(403);
    $expectedStatus = [
      'edit any entity in translation skills' => 200,
    ];
    $this->expectedStatus = array_merge($default_status, $expectedStatus);
    $this->runTests($user);
  }

  /**
   * Test delete entity in translation skills permissions.
   */
  public function testDeleteEntityInTranslationSkills() {
    $bundle = $this->bundle;
    $user = $this->createUser([
      "translators_content delete any $bundle media",
    ]);
    $default_status = $this->getDefaultExpectedStatus(403);
    $expectedStatus = [
      'delete any entity in translation skills' => 200,
    ];
    $this->expectedStatus = array_merge($default_status, $expectedStatus);
    $this->runTests($user);
  }

  /**
   * Create test node.
   *
   * @param null|string $langcode
   *   Optional. Default language ID.
   * @param null|int $uid
   *   Optional. Author ID.
   */
  public function createTestEntity($langcode = NULL, int $uid = NULL) {
    $langcode = !empty($langcode) ? $langcode : $this->getSiteDefaultLangcode();
    $uid = !empty($uid) ? $uid : \Drupal::currentUser()->id();
    $bundle = isset($this->bundle) ? $this->bundle : 'image';
    $media = Media::create([
      'bundle' => $bundle,
      'uid' => $uid,
      'status' => 1,
      'langcode' => $langcode,
      'field_media_image' => [
        'target_id' => 1,
        'alt' => $this->randomString(),
        'title' => $this->randomString(),
      ],
    ]);
    $media->save();
    return $media;
  }

  /**
   * {@inheritdoc}
   */
  public function addEntityTranslation($entity, $target, $source = NULL) {
    $source = !empty($source) ? $source : $entity->getUntranslated()->language()->getId();
    $translation_add_url = $this->getCreateEntityTranslationUrl($entity, $source, $target);
    $this->drupalGet($translation_add_url);
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([], t('Save'));
    $this->rebuildContainer();
  }

  /**
   * {@inheritdoc}
   */
  public function getCreateEntityUrl($langcode = NULL) {
    $bundle = $this->bundle;
    if (empty($langcode) || $langcode == $this->getSiteDefaultLangcode()) {
      return "/media/add/$bundle";
    }
    return "/$langcode/media/add/$bundle";
  }

}
