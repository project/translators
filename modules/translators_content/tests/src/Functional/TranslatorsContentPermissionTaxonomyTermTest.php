<?php

namespace Drupal\Tests\translators_content\Functional;

use Drupal\taxonomy\Entity\Term;

/**
 * Class TranslatorsContentPermissionNodeTest.
 *
 * @package Drupal\Tests\translators_content\Functional
 *
 * @group translators_content
 */
class TranslatorsContentPermissionTaxonomyTermTest extends TranslatorsContentPermissionTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['translators_content', 'taxonomy'];
  /**
   * {@inheritdoc}
   */
  protected $entityTypeId = 'taxonomy_term';
  /**
   * {@inheritdoc}
   */
  protected $bundle = 'tags';
  /**
   * {@inheritdoc}
   */
  protected $entityPermissionSupport = TRUE;

  /**
   * {@inheritdoc}
   */
  public function testTranslatonPermissionsVisability() {
    parent::testTranslatonPermissionsVisability();
    $this->runTestEntityPermissionsVisability();
  }

  /**
   * Test create entity any language permissions.
   */
  public function testCreateEntityAnyLanguage() {
    $bundle = $this->bundle;
    $user = $this->createUser([
      "create terms in $bundle",
    ]);
    $defaultStatus = $this->getDefaultExpectedStatus(403);
    $expectedStatus = [
      'create entity in any language' => 200,
      'create entity in translation skills' => 200,
    ];
    $this->expectedStatus = array_merge($defaultStatus, $expectedStatus);
    $this->runTests($user);
  }

  /**
   * Test edit entity any language permissions.
   */
  public function testEditAnyEntityAnyLanguage() {
    $bundle = $this->bundle;
    $user = $this->createUser([
      "edit terms in $bundle",
    ]);
    $defaultStatus = $this->getDefaultExpectedStatus(403);
    $expectedStatus = [
      'edit any entity in any language' => 200,
      'edit any entity in translation skills' => 200,
    ];
    $this->expectedStatus = array_merge($defaultStatus, $expectedStatus);
    $this->runTests($user);
  }

  /**
   * Test delete entity any language permissions.
   */
  public function testDeleteAnyEntityAnyLanguage() {
    $bundle = $this->bundle;
    $user = $this->createUser([
      "delete terms in $bundle",
    ]);
    $defaultStatus = $this->getDefaultExpectedStatus(403);
    $expectedStatus = [
      'delete any entity in any language' => 200,
      'delete any entity in translation skills' => 200,
      'delete own entity' => 200,
    ];
    $this->expectedStatus = array_merge($defaultStatus, $expectedStatus);
    $this->runTests($user);
  }

  /**
   * Test create entity in translation skills permissions.
   */
  public function testCreateEntityInTranslationSkills() {
    $bundle = $this->bundle;
    $user = $this->createUser([
      "translators_content create $bundle taxonomy_term",
    ]);
    $default_status = $this->getDefaultExpectedStatus(403);
    $expectedStatus = [
      'create entity in translation skills' => 200,
    ];
    $this->expectedStatus = array_merge($default_status, $expectedStatus);
    $this->runTests($user);
  }

  /**
   * Test edit entity in translation skills permissions.
   */
  public function testEditEntityInTranslationSkills() {
    $bundle = $this->bundle;
    $user = $this->createUser([
      "translators_content edit any $bundle taxonomy_term",
    ]);
    $default_status = $this->getDefaultExpectedStatus(403);
    $expectedStatus = [
      'edit any entity in translation skills' => 200,
    ];
    $this->expectedStatus = array_merge($default_status, $expectedStatus);
    $this->runTests($user);
  }

  /**
   * Test delete entity in translation skills permissions.
   */
  public function testDeleteEntityInTranslationSkills() {
    $bundle = $this->bundle;
    $user = $this->createUser([
      "translators_content delete any $bundle taxonomy_term",
    ]);
    $default_status = $this->getDefaultExpectedStatus(403);
    $expectedStatus = [
      'delete any entity in translation skills' => 200,
    ];
    $this->expectedStatus = array_merge($default_status, $expectedStatus);
    $this->runTests($user);
  }

  /**
   * Create test term.
   *
   * @param null|string $langcode
   *   Optional. Default language ID.
   * @param null|int $uid
   *   Optional. Author ID.
   */
  public function createTestEntity($langcode = NULL, int $uid = NULL) {
    $langcode = !empty($langcode) ? $langcode : $this->getSiteDefaultLangcode();
    $bundle = $this->bundle;
    // Create a vocabulary term.
    $term = Term::create([
      'name' => "Term $bundle $langcode",
      'vid' => $bundle,
      'langcode' => $langcode,
      'status' => 1,
    ]);
    $term->save();
    return $term;

  }

  /**
   * {@inheritdoc}
   */
  public function getCreateEntityUrl($langcode = NULL) {
    $bundle = $this->bundle;
    if (empty($langcode) || $langcode == $this->getSiteDefaultLangcode()) {
      return "/admin/structure/taxonomy/manage/$bundle/add";
    }
    return "/$langcode/admin/structure/taxonomy/manage/$bundle/add";
  }

  /**
   * {@inheritdoc}
   */
  public function addEntityTranslation($entity, $target, $source = NULL) {
    $source = !empty($source) ? $source : $entity->getUntranslated()->language()->getId();
    $translation_add_url = $this->getCreateEntityTranslationUrl($entity, $source, $target);
    $this->drupalGet($translation_add_url);
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([], t('Save'));
    $this->rebuildContainer();
  }

}
