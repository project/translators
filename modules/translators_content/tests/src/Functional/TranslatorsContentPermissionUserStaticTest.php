<?php

namespace Drupal\Tests\translators_content\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Class TranslatorsContentEntityPermissionsTest.
 *
 * @package Drupal\Tests\translators_content\Functional
 *
 * @group translators_content
 */
class TranslatorsContentPermissionUserStaticTest extends BrowserTestBase {
  use TranslatorsContentTestsTrait;

  /**
   * {@inheritdoc}
   */
  public $profile = 'standard';
  /**
   * {@inheritdoc}
   */
  protected static $modules = ['translators_content'];

  /**
   * Test that ensures we don't restrict access to the user's edit form.
   */
  public function testUserEditFormWorkaround() {
    $user = $this->createUser();
    $this->drupalLogin($user);

    // Check entity local task tabs existence.
    $this->drupalGet("user/{$user->id()}");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Edit\']/@href');

    // Check for the edit page access.
    $this->drupalGet("user/{$user->id()}/edit");
    $this->assertSession()->statusCodeEquals(200);

    // Additionally check that we don't give access to this form
    // for anonymous users.
    $this->drupalLogout();
    $this->drupalGet("user/{$user->id()}/edit");
    $this->assertSession()->statusCodeEquals(403);
  }

}
