<?php

namespace Drupal\Tests\translators_content\Functional;

use Drupal\translators_content\Plugin\views\filter\TranslationLanguageLimitedToTranslationSkills;
use Drupal\node\Entity\Node;
use Drupal\Tests\BrowserTestBase;

/**
 * Class TranslatorsContentViewsLanguageFilterTest.
 *
 * @package Drupal\Tests\translators_content\Functional
 *
 * @group translators_content
 */
class TranslatorsContentViewsLanguageFilterTest extends BrowserTestBase {
  use TranslatorsContentTestsTrait;

  /**
   * {@inheritdoc}
   */
  public $profile = 'standard';
  /**
   * {@inheritdoc}
   */
  protected static $modules = ['translators_content', 'translators_content_test_views'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->drupalLogin($this->rootUser);
    $this->createLanguages(['fr', 'de', 'sq']);
    $this->drupalLogout();
  }

  /**
   * Test Translators language filter altering.
   */
  public function testTranslatorsLanguageFilterPluginAltering() {
    $definition = $this->container
      ->get('plugin.manager.views.filter')
      ->getDefinition('language');

    $this->assertNotNull($definition);
    $this->assertArrayHasKey('plugin_type', $definition);
    $this->assertArrayHasKey('id', $definition);
    $this->assertArrayHasKey('class', $definition);
    $this->assertArrayHasKey('provider', $definition);

    $this->assertEquals('translators_content', $definition['provider']);
    $this->assertEquals(TranslationLanguageLimitedToTranslationSkills::class, $definition['class']);
    $this->assertEquals('language', $definition['id']);
    $this->assertEquals('filter', $definition['plugin_type']);
  }

  /**
   * Test Translators language filter in view.
   */
  public function testTranslatorsLanguageFilterInView() {
    $this->drupalLogin($this->rootUser);
    $this->addSkill(['en', 'fr']);
    for ($i = 1; $i <= 10; $i++) {
      Node::create([
        'type' => 'article',
        'title' => $this->randomString(),
        'langcode' => 'en',
      ])
        ->addTranslation('fr', ['title' => $this->randomString()])
        ->save();
    }

    // Test withouth language filtering enabled.
    $this->drupalGet('/test-translators-content-filter');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->statusCodeNotEquals(404);

    $this->assertOptionCount('langcode', 9);
    $this->assertOptionAvailable('langcode', 'All');
    $this->assertOptionAvailable('langcode', '***LANGUAGE_site_default***');
    $this->assertOptionAvailable('langcode', '***LANGUAGE_language_interface***');
    $this->assertOptionAvailable('langcode', 'und');
    $this->assertOptionAvailable('langcode', 'zxx');
    $this->assertOptionAvailable('langcode', 'en');
    $this->assertOptionAvailable('langcode', 'fr');
    $this->assertOptionAvailable('langcode', 'de');
    $this->assertOptionAvailable('langcode', 'sq');

    // Test with language filtering enabled.
    $this->drupalGet('/admin/structure/views/nojs/handler/test_translators_content_filter/page_1/filter/langcode');
    $this->submitForm([
      'options[limit]'          => 1,
      'options[column][source]' => 1,
      'options[column][target]' => 1,
    ], 'Apply');
    $this->click('input[value="Save"]');

    $this->drupalGet('/test-translators-content-filter');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->statusCodeNotEquals(404);

    $this->assertOptionCount('langcode', 3);
    $this->assertOptionAvailable('langcode', 'All');
    $this->assertOptionAvailable('langcode', 'en');
    $this->assertOptionAvailable('langcode', 'fr');
    $this->assertOptionNotAvailable('langcode', 'de');
    $this->assertOptionNotAvailable('langcode', 'sq');

  }

}
