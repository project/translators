<?php

namespace Drupal\Tests\translators_content\Functional;

use Drupal\block_content\Entity\BlockContent;

/**
 * Class TranslatorsContentPermissionBlockContentTest.
 *
 * @package Drupal\Tests\translators_content\Functional
 *
 * @group translators_content
 */
class TranslatorsContentPermissionBlockContentTest extends TranslatorsContentPermissionTestBase {
  use TranslatorsContentTestsTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['translators_content', 'block', 'block_content'];
  /**
   * {@inheritdoc}
   */
  protected $entityTypeId = 'block_content';
  /**
   * {@inheritdoc}
   */
  protected $bundle = 'basic';
  /**
   * {@inheritdoc}
   */
  protected $entityPermissionSupport = FALSE;

  /**
   * Create test node.
   *
   * @param null|string $langcode
   *   Optional. Default language ID.
   * @param null|int $uid
   *   Optional. Author ID.
   */
  public function createTestEntity($langcode = NULL, int $uid = NULL) {
    $langcode = !empty($langcode) ? $langcode : $this->getSiteDefaultLangcode();
    $bundle = $this->bundle;
    $block_content = BlockContent::create([
      'info' => $this->randomString(),
      'type' => $bundle,
      'langcode' => $langcode,
    ]);
    $block_content->save();
    return $block_content;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreateEntityUrl($langcode = NULL) {
    if (empty($langcode) || $langcode == $this->getSiteDefaultLangcode()) {
      return "/block/add";
    }
    return "/$langcode/block/add";
  }

  /**
   * {@inheritdoc}
   */
  public function addEntityTranslation($entity, $target, $source = NULL) {
    $source = !empty($source) ? $source : $entity->getUntranslated()->language()->getId();
    $translation_add_url = $this->getCreateEntityTranslationUrl($entity, $source, $target);
    $this->drupalGet($translation_add_url);
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([], t('Save'));
    $this->assertSession()->statusCodeEquals(200);
    $this->rebuildContainer();
  }

  /**
   * {@inheritdoc}
   */
  public function runTestLocalTaskTabsExistence($entity, $label, $url, $langcode = NULL) {
    // Do not run tests as entity is not visable for non-admin users.
  }

  /**
   * {@inheritdoc}
   */
  public function runTestNoLocalTaskTabsExistence($entity, $label, $langcode = NULL) {
    // Do not run tests as entity is not visable for non-admin users.
  }

}
