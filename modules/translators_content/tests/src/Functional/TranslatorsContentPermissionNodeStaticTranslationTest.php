<?php

namespace Drupal\Tests\translators_content\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Class TranslatorsContentEntityPermissionsTest.
 *
 * @package Drupal\Tests\translators_content\Functional
 *
 * @group translators_content
 */
class TranslatorsContentPermissionNodeStaticTranslationTest extends BrowserTestBase {
  use TranslatorsContentTestsTrait;

  /**
   * {@inheritdoc}
   */
  public $profile = 'standard';

  /**
   * The default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'claro';

  /**
   * The administration theme.
   *
   * @var string
   */
  protected $adminTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['translators_content'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->drupalLogin($this->rootUser);
    $theme_installer = $this->container->get('theme_installer');
    $theme_installer->install([$this->defaultTheme, $this->adminTheme]);
    $this->config('system.theme')
      ->set('default', $this->defaultTheme)
      ->set('admin', $this->adminTheme)
      ->save();
    
    $this->drupalLogin($this->rootUser);
    $this->createLanguages(['fr', 'de', 'sq']);
    $this->enableTranslation('node', 'article');
    $this->enableFilterTranslationOverviewToSkills(FALSE);
    $this->drupalLogout();
  }

  /**
   * Test core content_translation "create" access.
   */
  public function testCoreCreateTranslation() {
    $nid = $this->createTestNode('en')->id();

    $translationCreator1 = $this->createUser([
      'create content translations',
      'translate article node',
    ]);
    $this->drupalLogin($translationCreator1);
    // Local task item.
    $this->drupalGet("node/$nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Translate\']/@href');
    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementNotExists('css', '.add a[hreflang="en"]');
    $this->assertSession()
      ->elementExists('css', '.add a[hreflang="fr"]');
    $this->assertSession()
      ->elementExists('css', '.add a[hreflang="de"]');
    $this->assertSession()
      ->elementExists('css', '.add a[hreflang="sq"]');
    // Direct links.
    $this->drupalGet("node/$nid/translations/add/en/fr");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("node/$nid/translations/add/en/de");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("node/$nid/translations/add/en/sq");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogout();

    $translationCreator2 = $this->createUser([
      'create content translations',
      'translate any entity',
    ]);
    $this->drupalLogin($translationCreator2);
    // Local task item.
    $this->drupalGet("node/$nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Translate\']/@href');
    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementNotExists('css', '.add a[hreflang="en"]');
    $this->assertSession()
      ->elementExists('css', '.add a[hreflang="fr"]');
    $this->assertSession()
      ->elementExists('css', '.add a[hreflang="de"]');
    $this->assertSession()
      ->elementExists('css', '.add a[hreflang="sq"]');
    // Direct links.
    $this->drupalGet("node/$nid/translations/add/en/fr");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("node/$nid/translations/add/en/de");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("node/$nid/translations/add/en/sq");
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test translators_content "create" access.
   */
  public function testCreateTranslationTranslationSkills() {
    $nid = $this->createTestNode('en')->id();

    $translationCreator1 = $this->createUser([
      'translators_content create content translations',
      'translate article node',
    ]);
    $this->drupalLogin($translationCreator1);
    $this->addSkill(['en', 'fr']);
    // Local task item.
    $this->drupalGet("node/$nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Translate\']/@href');
    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementNotExists('css', '.add a[hreflang="en"]');
    $this->assertSession()
      ->elementExists('css', '.add a[hreflang="fr"]');
    $this->assertSession()
      ->elementNotExists('css', '.add a[hreflang="de"]');
    $this->assertSession()
      ->elementNotExists('css', '.add a[hreflang="sq"]');
    // Direct links.
    $this->drupalGet("node/$nid/translations/add/en/fr");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("node/$nid/translations/add/en/de");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("node/$nid/translations/add/en/sq");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalLogout();

    $translationCreator2 = $this->createUser([
      'translators_content create content translations',
      'translate any entity',
    ]);
    $this->drupalLogin($translationCreator2);
    $this->addSkill(['en', 'fr']);
    // Local task item.
    $this->drupalGet("node/$nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Translate\']/@href');
    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementNotExists('css', '.add a[hreflang="en"]');
    $this->assertSession()
      ->elementExists('css', '.add a[hreflang="fr"]');
    $this->assertSession()
      ->elementNotExists('css', '.add a[hreflang="de"]');
    $this->assertSession()
      ->elementNotExists('css', '.add a[hreflang="sq"]');
    // Direct links.
    $this->drupalGet("node/$nid/translations/add/en/fr");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("node/$nid/translations/add/en/de");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("node/$nid/translations/add/en/sq");
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test "create" access source language.
   */
  public function testCreateTranslationSourceLanguage() {
    $this->drupalLogin($this->rootUser);
    // Settings.
    $this->enableStrictTranslationSkillsPairing(FALSE);
    $this->enableAutoPresetSourceLanguage(FALSE);
    // Create test node.
    $node = $this->createTestNode('en');
    $nid = $node->id();
    $this->addNodeTranslation($node, 'de');
    $this->addNodeTranslation($node, 'sq');
    $this->drupalLogout();

    // Test with core create translation access.
    $translationCreator1 = $this->createUser([
      'create content translations',
      'translate article node',
    ]);
    $this->drupalLogin($translationCreator1);
    $this->drupalGet("node/$nid/translations/add/en/fr");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertOptionCount('source_langcode[source]', 3);
    $this->assertOptionAvailable('source_langcode[source]', 'en');
    $this->assertOptionAvailable('source_langcode[source]', 'de');
    $this->assertOptionAvailable('source_langcode[source]', 'sq');
    $this->assertOptionNotAvailable('source_langcode[source]', 'fr');

    $this->drupalGet("node/$nid/translations/add/de/fr");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertOptionCount('source_langcode[source]', 3);
    $this->assertOptionAvailable('source_langcode[source]', 'en');
    $this->assertOptionAvailable('source_langcode[source]', 'de');
    $this->assertOptionAvailable('source_langcode[source]', 'sq');
    $this->assertOptionNotAvailable('source_langcode[source]', 'fr');

    $this->drupalGet("node/$nid/translations/add/sq/fr");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertOptionCount('source_langcode[source]', 3);
    $this->assertOptionAvailable('source_langcode[source]', 'en');
    $this->assertOptionAvailable('source_langcode[source]', 'de');
    $this->assertOptionAvailable('source_langcode[source]', 'sq');
    $this->assertOptionNotAvailable('source_langcode[source]', 'fr');

    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementExists('css', '.add a[hreflang="fr"]');
    $this->click('.add a[hreflang="fr"]');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals("/fr/node/$nid/translations/add/en/fr");
    $this->drupalLogout();

    // Test with translators_content create translation access.
    $translationCreator2 = $this->createUser([
      'translators_content create content translations',
      'translate article node',
    ]);
    $this->drupalLogin($translationCreator2);
    $this->addSkill(['en', 'fr']);
    $this->addSkill(['de', 'fr']);
    $this->addSkill(['en', 'sq']);
    $this->drupalGet("node/$nid/translations/add/en/fr");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertOptionCount('source_langcode[source]', 3);
    $this->assertOptionAvailable('source_langcode[source]', 'en');
    $this->assertOptionAvailable('source_langcode[source]', 'de');
    $this->assertOptionAvailable('source_langcode[source]', 'sq');
    $this->assertOptionNotAvailable('source_langcode[source]', 'fr');

    $this->drupalGet("node/$nid/translations/add/de/fr");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertOptionCount('source_langcode[source]', 3);
    $this->assertOptionAvailable('source_langcode[source]', 'en');
    $this->assertOptionAvailable('source_langcode[source]', 'de');
    $this->assertOptionAvailable('source_langcode[source]', 'sq');
    $this->assertOptionNotAvailable('source_langcode[source]', 'fr');

    $this->drupalGet("node/$nid/translations/add/sq/fr");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertOptionCount('source_langcode[source]', 3);
    $this->assertOptionAvailable('source_langcode[source]', 'en');
    $this->assertOptionAvailable('source_langcode[source]', 'de');
    $this->assertOptionAvailable('source_langcode[source]', 'sq');
    $this->assertOptionNotAvailable('source_langcode[source]', 'fr');

    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementExists('css', '.add a[hreflang="fr"]');
    $this->click('.add a[hreflang="fr"]');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals("/fr/node/$nid/translations/add/en/fr");
    $this->drupalLogout();

    // Test with strict translation skill pairing.
    $this->drupalLogin($this->rootUser);
    $this->enableStrictTranslationSkillsPairing(TRUE);
    $this->drupalLogout();

    $this->drupalLogin($translationCreator2);
    $this->drupalGet("node/$nid/translations/add/en/fr");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertOptionCount('source_langcode[source]', 2);
    $this->assertOptionAvailable('source_langcode[source]', 'en');
    $this->assertOptionAvailable('source_langcode[source]', 'de');
    $this->assertOptionNotAvailable('source_langcode[source]', 'sq');
    $this->assertOptionNotAvailable('source_langcode[source]', 'fr');

    $this->drupalGet("node/$nid/translations/add/de/fr");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertOptionCount('source_langcode[source]', 2);
    $this->assertOptionAvailable('source_langcode[source]', 'en');
    $this->assertOptionAvailable('source_langcode[source]', 'de');
    $this->assertOptionNotAvailable('source_langcode[source]', 'sq');
    $this->assertOptionNotAvailable('source_langcode[source]', 'fr');

    $this->drupalGet("node/$nid/translations/add/sq/fr");
    $this->assertSession()->statusCodeEquals(403);

    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementExists('css', '.add a[hreflang="fr"]');
    $this->click('.add a[hreflang="fr"]');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals("/fr/node/$nid/translations/add/en/fr");
    $this->drupalLogout();

    // Test preset source language.
    $this->drupalLogin($this->rootUser);
    $this->enableAutoPresetSourceLanguage(TRUE);
    $this->drupalLogout();

    // Test with core create translation access.
    $translationCreator3 = $this->createUser([
      'create content translations',
      'translate article node',
    ]);
    $this->drupalLogin($translationCreator3);
    $this->addSkill(['de', 'fr']);
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementExists('css', '.add a[hreflang="fr"]');
    $this->click('.add a[hreflang="fr"]');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals("/fr/node/$nid/translations/add/de/fr");
    $this->drupalLogout();

    // Test with translators_content create translation access.
    $translationCreator4 = $this->createUser([
      'translators_content create content translations',
      'translate article node',
    ]);
    $this->drupalLogin($translationCreator4);
    $this->addSkill(['de', 'fr']);
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementExists('css', '.add a[hreflang="fr"]');
    $this->click('.add a[hreflang="fr"]');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals("/fr/node/$nid/translations/add/de/fr");
  }

  /**
   * Test core content_translation "edit" access.
   */
  public function testCoreEditTranslation() {
    $this->drupalLogin($this->rootUser);
    $node = $this->createTestNode('en');
    $nid = $node->id();
    $this->addAllNodeTranslations($node);
    $this->drupalLogout();

    $translationEditor1 = $this->createUser([
      'update content translations',
      'translate article node',
    ]);
    $this->drupalLogin($translationEditor1);
    // Local task item.
    $this->drupalGet("node/$nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Translate\']/@href');
    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementNotExists('css', '.edit a[hreflang="en"]');
    $this->assertSession()
      ->elementExists('css', '.edit a[hreflang="fr"]');
    $this->assertSession()
      ->elementExists('css', '.edit a[hreflang="de"]');
    $this->assertSession()
      ->elementExists('css', '.edit a[hreflang="sq"]');
    // Direct links.
    $this->drupalGet("node/$nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$nid/edit");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("de/node/$nid/edit");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("sq/node/$nid/edit");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogout();

    $translationEditor2 = $this->createUser([
      'update content translations',
      'translate any entity',
    ]);
    $this->drupalLogin($translationEditor2);
    // Local task item.
    $this->drupalGet("node/$nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Translate\']/@href');
    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementNotExists('css', '.edit a[hreflang="en"]');
    $this->assertSession()
      ->elementExists('css', '.edit a[hreflang="fr"]');
    $this->assertSession()
      ->elementExists('css', '.edit a[hreflang="de"]');
    $this->assertSession()
      ->elementExists('css', '.edit a[hreflang="sq"]');
    // Direct links.
    $this->drupalGet("node/$nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$nid/edit");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("de/node/$nid/edit");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("sq/node/$nid/edit");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogout();
  }

  /**
   * Test translators_content "edit" access.
   */
  public function testEditTranslationTranslationSkills() {
    $this->drupalLogin($this->rootUser);
    $node = $this->createTestNode('en');
    $nid = $node->id();
    $this->addAllNodeTranslations($node);
    $this->drupalLogout();

    $translationEditor1 = $this->createUser([
      'translators_content update content translations',
      'translate article node',
    ]);
    $this->drupalLogin($translationEditor1);
    $this->addSkill(['en', 'fr']);
    // Local task item.
    $this->drupalGet("node/$nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Translate\']/@href');
    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementNotExists('css', '.edit a[hreflang="en"]');
    $this->assertSession()
      ->elementExists('css', '.edit a[hreflang="fr"]');
    $this->assertSession()
      ->elementNotExists('css', '.edit a[hreflang="de"]');
    $this->assertSession()
      ->elementNotExists('css', '.edit a[hreflang="sq"]');
    // Direct links.
    $this->drupalGet("node/$nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$nid/edit");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("de/node/$nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("sq/node/$nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalLogout();

    $translationEditor2 = $this->createUser([
      'translators_content update content translations',
      'translate any entity',
    ]);
    $this->drupalLogin($translationEditor2);
    $this->addSkill(['en', 'fr']);
    // Local task item.
    $this->drupalGet("node/$nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Translate\']/@href');
    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementNotExists('css', '.edit a[hreflang="en"]');
    $this->assertSession()
      ->elementExists('css', '.edit a[hreflang="fr"]');
    $this->assertSession()
      ->elementNotExists('css', '.edit a[hreflang="de"]');
    $this->assertSession()
      ->elementNotExists('css', '.edit a[hreflang="sq"]');
    // Direct links.
    $this->drupalGet("node/$nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$nid/edit");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("de/node/$nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("sq/node/$nid/edit");
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test core content_translation "delete" access.
   */
  public function testCoreDeleteTranslation() {
    $this->drupalLogin($this->rootUser);
    $node = $this->createTestNode('en');
    $nid = $node->id();
    $this->addAllNodeTranslations($node);
    $this->drupalLogout();

    $translationDeleter1 = $this->createUser([
      'delete content translations',
      'translate article node',
    ]);
    $this->drupalLogin($translationDeleter1);
    // Local task item.
    $this->drupalGet("node/$nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Translate\']/@href');
    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementNotExists('css', '.delete a[hreflang="en"]');
    $this->assertSession()
      ->elementExists('css', '.delete a[hreflang="fr"]');
    $this->assertSession()
      ->elementExists('css', '.delete a[hreflang="de"]');
    $this->assertSession()
      ->elementExists('css', '.delete a[hreflang="sq"]');
    // Direct links.
    $this->drupalGet("node/$nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$nid/delete");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("de/node/$nid/delete");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("sq/node/$nid/delete");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogout();

    $translationDeleter2 = $this->createUser([
      'delete content translations',
      'translate any entity',
    ]);
    $this->drupalLogin($translationDeleter2);
    // Local task item.
    $this->drupalGet("node/$nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Translate\']/@href');
    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementNotExists('css', '.delete a[hreflang="en"]');
    $this->assertSession()
      ->elementExists('css', '.delete a[hreflang="fr"]');
    $this->assertSession()
      ->elementExists('css', '.delete a[hreflang="de"]');
    $this->assertSession()
      ->elementExists('css', '.delete a[hreflang="sq"]');
    // Direct links.
    $this->drupalGet("node/$nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$nid/delete");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("de/node/$nid/delete");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("sq/node/$nid/delete");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogout();
  }

  /**
   * Test translators_content "delete" access.
   */
  public function testDeleteTranslationTranslationSkills() {
    $this->drupalLogin($this->rootUser);
    $node = $this->createTestNode('en');
    $nid = $node->id();
    $this->addAllNodeTranslations($node);
    $this->drupalLogout();

    $translationDeleter1 = $this->createUser([
      'translators_content delete content translations',
      'translate article node',
    ]);
    $this->drupalLogin($translationDeleter1);
    $this->addSkill(['en', 'fr']);
    // Local task item.
    $this->drupalGet("node/$nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Translate\']/@href');
    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementNotExists('css', '.delete a[hreflang="en"]');
    $this->assertSession()
      ->elementExists('css', '.delete a[hreflang="fr"]');
    $this->assertSession()
      ->elementNotExists('css', '.delete a[hreflang="de"]');
    $this->assertSession()
      ->elementNotExists('css', '.delete a[hreflang="sq"]');
    // Direct links.
    $this->drupalGet("node/$nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$nid/delete");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("de/node/$nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("sq/node/$nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalLogout();

    $translationDeleter2 = $this->createUser([
      'translators_content delete content translations',
      'translate any entity',
    ]);
    $this->drupalLogin($translationDeleter2);
    $this->addSkill(['en', 'fr']);
    // Local task item.
    $this->drupalGet("node/$nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Translate\']/@href');
    // Translation overview.
    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()
      ->elementNotExists('css', '.delete a[hreflang="en"]');
    $this->assertSession()
      ->elementExists('css', '.delete a[hreflang="fr"]');
    $this->assertSession()
      ->elementNotExists('css', '.delete a[hreflang="de"]');
    $this->assertSession()
      ->elementNotExists('css', '.delete a[hreflang="sq"]');
    // Direct links.
    $this->drupalGet("node/$nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$nid/delete");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("de/node/$nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("sq/node/$nid/delete");
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test translation overview.
   */
  public function testTranslationOverview() {
    $this->drupalLogin($this->rootUser);
    $node = $this->createTestNode('en');
    $nid = $node->id();
    $this->addAllNodeTranslations($node);
    $this->drupalLogout();

    $translation_editor = $this->createUser([
      'translators_content update content translations',
      'translators_content delete content translations',
      'translate article node',
    ]);
    $this->drupalLogin($translation_editor);
    $this->addSkill(['en', 'fr']);

    $this->drupalGet("node/$nid/translations");
    $this->assertSession()->statusCodeEquals(200);

    $language_column_css = 'main table td:first-child';
    // Test node default language.
    $this->assertAnyElementContains('css', $language_column_css, 'English (Original language)');
    $this->assertSession()
      ->elementNotExists('css', '.edit a[hreflang="en"]');
    $this->assertSession()
      ->elementNotExists('css', '.delete a[hreflang="en"]');
    // Test French translation (in translation skills).
    $this->assertAnyElementContains('css', $language_column_css, 'French');
    $this->assertSession()
      ->elementExists('css', '.edit a[hreflang="fr"]');
    $this->assertSession()
      ->elementExists('css', '.delete a[hreflang="fr"]');

    // Test other languages.
    $this->assertAnyElementContains('css', $language_column_css, 'German');
    $this->assertSession()
      ->elementNotExists('css', '.edit a[hreflang="de"]');
    $this->assertSession()
      ->elementNotExists('css', '.delete a[hreflang="de"]');
    $this->assertAnyElementContains('css', $language_column_css, 'Albanian');
    $this->assertSession()
      ->elementNotExists('css', '.edit a[hreflang="sq"]');
    $this->assertSession()
      ->elementNotExists('css', '.delete a[hreflang="sq"]');
    $this->drupalLogout();

    // Test with language filtering enabled.
    $this->drupalLogin($this->rootUser);
    $this->enableFilterTranslationOverviewToSkills(TRUE);
    $this->drupalLogout();
    $this->drupalLogin($translation_editor);

    $this->drupalGet("node/$nid/translations");
    $this->assertSession()
      ->elementContains('css', 'main #show-more-translations-link', 'Show all languages');
    $this->assertNoneElementContains('css', $language_column_css, 'German');
    $this->assertNoneElementContains('css', $language_column_css, 'Albanian');

  }

}
