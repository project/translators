<?php

namespace Drupal\Tests\translators_content\Functional;

use Drupal\comment\Entity\Comment;
use Drupal\node\Entity\Node;

/**
 * Class TranslatorsContentPermissionCommentTest.
 *
 * @package Drupal\Tests\translators_content\Functional
 *
 * @group translators_content
 */
class TranslatorsContentPermissionCommentTest extends TranslatorsContentPermissionTestBase {
  use TranslatorsContentTestsTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['translators_content', 'node', 'comment'];
  /**
   * {@inheritdoc}
   */
  protected $entityTypeId = 'comment';
  /**
   * {@inheritdoc}
   */
  protected $bundle = 'comment';
  /**
   * {@inheritdoc}
   */
  protected $entityPermissionSupport = FALSE;
  /**
   * A node to test comments on.
   *
   * @var Drupal\node\Entity\Node
   */
  protected $testNode;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $node = Node::create([
      'type' => 'article',
      'title' => $this->randomString(),
      'langcode' => 'en',
      'uid' => 1,
      'status' => 1,
    ]);
    $node->save();
    $this->testNode = $node;
  }

  /**
   * Create test node.
   *
   * @param null|string $langcode
   *   Optional. Default language ID.
   * @param null|int $uid
   *   Optional. Author ID.
   */
  public function createTestEntity($langcode = NULL, int $uid = NULL) {
    $langcode = !empty($langcode) ? $langcode : $this->getSiteDefaultLangcode();
    $uid = !empty($uid) ? $uid : \Drupal::currentUser()->id();
    $nid = $this->testNode->id();
    $comment = Comment::create([
      'subject' => $this->randomString(),
      'comment_body' => $this->randomString(),
      'uid' => $uid,
      'entity_type' => 'node',
      'entity_id' => $nid,
      'comment_type' => 'comment',
      'field_name' => 'comment',
      'status' => 1,
    ]);
    $comment->save();
    return $comment;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreateEntityUrl($langcode = NULL) {
    $nid = $this->testNode->id();
    if (empty($langcode) || $langcode == $this->getSiteDefaultLangcode()) {
      return "/comment/reply/node/$nid/comment";
    }
    return "/$langcode/comment/reply/node/$nid/comment";
  }

  /**
   * {@inheritdoc}
   */
  public function addEntityTranslation($entity, $target, $source = NULL) {
    $source = !empty($source) ? $source : $entity->getUntranslated()->language()->getId();
    $translation_add_url = $this->getCreateEntityTranslationUrl($entity, $source, $target);
    $this->drupalGet($translation_add_url);
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([], t('Save'));
    $this->assertSession()->statusCodeEquals(200);
    $this->rebuildContainer();
  }

  /**
   * {@inheritdoc}
   */
  public function runTestLocalTaskTabsExistence($entity, $label, $url, $langcode = NULL) {
    $this->drupalGetEntity($entity, $langcode);
    $this->assertSession()->statusCodeEquals(200);
    $id = $entity->id();
    $operation = $label == 'Translate' ? 'translations' : strtolower($label);
    $link = $this->assertSession()->elementExists('xpath', "//article[@id='comment-$id']//a[text()=\"$label\"]");
    $link->click();
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($url->toString());
  }

  /**
   * {@inheritdoc}
   */
  public function runTestNoLocalTaskTabsExistence($entity, $label, $langcode = NULL) {
    $this->drupalGetEntity($entity, $langcode);
    $this->assertSession()->statusCodeEquals(200);
    $id = $entity->id();
    $operation = $label == 'Translate' ? 'translations' : strtolower($label);
    $this->assertSession()->elementNotExists('xpath', "//article[@id='comment-$id']//a[text()=\"$label\"]");
  }

}
