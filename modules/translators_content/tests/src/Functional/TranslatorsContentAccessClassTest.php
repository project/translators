<?php

namespace Drupal\Tests\translators_content\Functional;

use Drupal\translators_content\Access\TranslatorsContentManageAccessCheck;
use Drupal\Tests\BrowserTestBase;

/**
 * Class TranslatorsContentAccessClassTest.
 *
 * @package Drupal\Tests\translators_content\Functional
 *
 * @group translators_content
 */
class TranslatorsContentAccessClassTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public $profile = 'standard';
  /**
   * {@inheritdoc}
   */
  protected static $modules = ['translators_content'];
  /**
   * Access rules manager.
   *
   * @var \Drupal\translators_content\Access\TranslatorsContentManageAccessCheck
   */
  protected $accessManager;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->accessManager = $this->container
      ->get('content_translation.manage_access');
  }

  /**
   * Test access rules manager existence.
   */
  public function testAccessManagerExistence() {
    $this->assertTrue($this->container->has('content_translation.manage_access'));
    $this->assertInstanceOf(TranslatorsContentManageAccessCheck::class, $this->accessManager);
  }

}
