<?php

namespace Drupal\Tests\translators_content\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Core\Language\Language;

/**
 * Class TranslatorsContentEntityPermissionsTest.
 *
 * @package Drupal\Tests\translators_content\Functional
 *
 * @group translators_content
 */
class TranslatorsContentPermissionNodeStaticEntityTest extends BrowserTestBase {
  use TranslatorsContentTestsTrait;

  /**
   * {@inheritdoc}
   */
  public $profile = 'standard';
  /**
   * {@inheritdoc}
   */
  protected static $modules = ['translators_content'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->drupalLogin($this->rootUser);
    $this->createLanguages(['fr', 'de', 'sq']);
    $this->enableTranslation('node', 'article');
    $this->drupalLogout();
  }

  /**
   * Test core "Create article node" permission.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testCreateEntityAnyLanguage() {
    $creator = $this->createUser(['create article content']);
    $this->drupalLogin($creator);
    $this->addSkill(['en', 'fr']);

    // Test access languages in translation skills.
    $this->drupalGet('node/add/article');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertOptionCount('langcode[0][value]', 6);
    $this->assertOptionAvailable('langcode[0][value]', 'en');
    $this->assertOptionAvailable('langcode[0][value]', 'fr');
    $this->assertOptionAvailable('langcode[0][value]', 'de');
    $this->assertOptionAvailable('langcode[0][value]', 'sq');
    $this->assertOptionAvailable('langcode[0][value]', Language::LANGCODE_NOT_SPECIFIED);
    $this->assertOptionAvailable('langcode[0][value]', Language::LANGCODE_NOT_APPLICABLE);

    $this->drupalGet('fr/node/add/article');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertOptionCount('langcode[0][value]', 6);
    $this->assertOptionAvailable('langcode[0][value]', 'en');
    $this->assertOptionAvailable('langcode[0][value]', 'fr');
    $this->assertOptionAvailable('langcode[0][value]', 'de');
    $this->assertOptionAvailable('langcode[0][value]', 'sq');
    $this->assertOptionAvailable('langcode[0][value]', Language::LANGCODE_NOT_SPECIFIED);
    $this->assertOptionAvailable('langcode[0][value]', Language::LANGCODE_NOT_APPLICABLE);

    // Test access other languages.
    $this->drupalGet('de/node/add/article');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertOptionCount('langcode[0][value]', 6);
    $this->assertOptionAvailable('langcode[0][value]', 'en');
    $this->assertOptionAvailable('langcode[0][value]', 'fr');
    $this->assertOptionAvailable('langcode[0][value]', 'de');
    $this->assertOptionAvailable('langcode[0][value]', 'sq');
    $this->assertOptionAvailable('langcode[0][value]', Language::LANGCODE_NOT_SPECIFIED);
    $this->assertOptionAvailable('langcode[0][value]', Language::LANGCODE_NOT_APPLICABLE);

    $this->drupalGet('sq/node/add/article');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertOptionCount('langcode[0][value]', 6);
    $this->assertOptionAvailable('langcode[0][value]', 'en');
    $this->assertOptionAvailable('langcode[0][value]', 'fr');
    $this->assertOptionAvailable('langcode[0][value]', 'de');
    $this->assertOptionAvailable('langcode[0][value]', 'sq');
    $this->assertOptionAvailable('langcode[0][value]', Language::LANGCODE_NOT_SPECIFIED);
    $this->assertOptionAvailable('langcode[0][value]', Language::LANGCODE_NOT_APPLICABLE);
  }

  /**
   * Test translators_content "create article node" permission.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testCreateEntityInTranslationSkillPermission() {
    $creator = $this->createUser(['translators_content create article node']);
    $this->drupalLogin($creator);
    $this->addSkill(['en', 'fr']);

    // Test access languages in translation skills.
    $this->drupalGet('node/add/article');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertOptionCount('langcode[0][value]', 4);
    $this->assertOptionAvailable('langcode[0][value]', 'en');
    $this->assertOptionAvailable('langcode[0][value]', 'fr');
    $this->assertOptionAvailable('langcode[0][value]', Language::LANGCODE_NOT_SPECIFIED);
    $this->assertOptionAvailable('langcode[0][value]', Language::LANGCODE_NOT_APPLICABLE);

    $this->drupalGet('fr/node/add/article');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertOptionCount('langcode[0][value]', 4);
    $this->assertOptionAvailable('langcode[0][value]', 'en');
    $this->assertOptionAvailable('langcode[0][value]', 'fr');
    $this->assertOptionAvailable('langcode[0][value]', Language::LANGCODE_NOT_SPECIFIED);
    $this->assertOptionAvailable('langcode[0][value]', Language::LANGCODE_NOT_APPLICABLE);

    // Test access other languages.
    $this->drupalGet('de/node/add/article');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied', FALSE);

    $this->drupalGet('sq/node/add/article');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->pageTextContains('Access denied', FALSE);
  }

  /**
   * Test core "edit any article node" permission.
   */
  public function testEditEntityAnyLanguagePermission() {
    $this->drupalLogin($this->rootUser);
    $node_en = $this->createTestNode('en');
    $this->addAllNodeTranslations($node_en);
    $node_fr = $this->createTestNode('fr');
    $this->addAllNodeTranslations($node_fr);
    $node_de = $this->createTestNode('de');
    $this->addAllNodeTranslations($node_de);
    $node_sq = $this->createTestNode('sq');
    $this->addAllNodeTranslations($node_sq);
    $this->drupalLogout();

    $editor = $this->createUser(['edit any article content']);
    $this->drupalLogin($editor);
    $this->addSkill(['en', 'fr']);

    // Test access in translation skills.
    $en_nid = $node_en->id();
    $this->drupalGet("node/$en_nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("node/$en_nid/edit");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("fr/node/$en_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("fr/node/$en_nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("de/node/$en_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("de/node/$en_nid/edit");
    $this->assertSession()->statusCodeEquals(403);

    $fr_nid = $node_fr->id();
    $this->drupalGet("node/$fr_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("node/$fr_nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$fr_nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("fr/node/$fr_nid/edit");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("de/node/$fr_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("de/node/$fr_nid/edit");
    $this->assertSession()->statusCodeEquals(403);

    // Test access in another language.
    $de_nid = $node_de->id();
    $this->drupalGet("node/$de_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("node/$de_nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$de_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("fr/node/$de_nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("de/node/$de_nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("de/node/$de_nid/edit");
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test translators_content "edit any article node" permission.
   */
  public function testEditEntityInTranslationSkillPermission() {
    $this->drupalLogin($this->rootUser);
    $node_en = $this->createTestNode('en');
    $this->addAllNodeTranslations($node_en);
    $node_fr = $this->createTestNode('fr');
    $this->addAllNodeTranslations($node_fr);
    $node_de = $this->createTestNode('de');
    $this->addAllNodeTranslations($node_de);
    $node_sq = $this->createTestNode('sq');
    $this->addAllNodeTranslations($node_sq);
    $this->drupalLogout();

    $editor = $this->createUser(['translators_content edit any article node']);
    $this->drupalLogin($editor);
    $this->addSkill(['en', 'fr']);

    // Test access in translation skills.
    $en_nid = $node_en->id();
    $this->drupalGet("node/$en_nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("node/$en_nid/edit");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("fr/node/$en_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("fr/node/$en_nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("de/node/$en_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("de/node/$en_nid/edit");
    $this->assertSession()->statusCodeEquals(403);

    $fr_nid = $node_fr->id();
    $this->drupalGet("node/$fr_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("node/$fr_nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$fr_nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("fr/node/$fr_nid/edit");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("de/node/$fr_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("de/node/$fr_nid/edit");
    $this->assertSession()->statusCodeEquals(403);

    // Test access in another language.
    $de_nid = $node_de->id();
    $this->drupalGet("node/$de_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("node/$de_nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$de_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("fr/node/$de_nid/edit");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("de/node/$de_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Edit\']/@href');
    $this->drupalGet("de/node/$de_nid/edit");
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test core "delete any article node" permission.
   */
  public function testDeleteEntityAnyLanguagePermission() {
    $this->drupalLogin($this->rootUser);
    $node_en = $this->createTestNode('en');
    $this->addAllNodeTranslations($node_en);
    $node_fr = $this->createTestNode('fr');
    $this->addAllNodeTranslations($node_fr);
    $node_de = $this->createTestNode('de');
    $this->addAllNodeTranslations($node_de);
    $node_sq = $this->createTestNode('sq');
    $this->addAllNodeTranslations($node_sq);
    $this->drupalLogout();

    $deleter = $this->createUser(['delete any article content']);
    $this->drupalLogin($deleter);
    $this->addSkill(['en', 'fr']);

    // Test access in translation skills.
    $en_nid = $node_en->id();
    $this->drupalGet("node/$en_nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("node/$en_nid/delete");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("fr/node/$en_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("fr/node/$en_nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("de/node/$en_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("de/node/$en_nid/delete");
    $this->assertSession()->statusCodeEquals(403);

    $fr_nid = $node_fr->id();
    $this->drupalGet("node/$fr_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("node/$fr_nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$fr_nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("fr/node/$fr_nid/delete");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("de/node/$fr_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("de/node/$fr_nid/delete");
    $this->assertSession()->statusCodeEquals(403);

    // Test access in another language.
    $de_nid = $node_de->id();
    $this->drupalGet("node/$de_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("node/$de_nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$de_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("fr/node/$de_nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("de/node/$de_nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("de/node/$de_nid/delete");
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test translators_content "delete any article node" permission.
   */
  public function testDeleteEntityInTranslationSkillPermission() {
    $this->drupalLogin($this->rootUser);
    $node_en = $this->createTestNode('en');
    $this->addAllNodeTranslations($node_en);
    $node_fr = $this->createTestNode('fr');
    $this->addAllNodeTranslations($node_fr);
    $node_de = $this->createTestNode('de');
    $this->addAllNodeTranslations($node_de);
    $node_sq = $this->createTestNode('sq');
    $this->addAllNodeTranslations($node_sq);
    $this->drupalLogout();

    $deleter = $this->createUser(['translators_content delete any article node']);
    $this->drupalLogin($deleter);
    $this->addSkill(['en', 'fr']);

    // Test access in translation skills.
    $en_nid = $node_en->id();
    $this->drupalGet("node/$en_nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("node/$en_nid/delete");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("fr/node/$en_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("fr/node/$en_nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("de/node/$en_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("de/node/$en_nid/delete");
    $this->assertSession()->statusCodeEquals(403);

    $fr_nid = $node_fr->id();
    $this->drupalGet("node/$fr_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("node/$fr_nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$fr_nid");
    $this->assertSession()->elementExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("fr/node/$fr_nid/delete");
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet("de/node/$fr_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("de/node/$fr_nid/delete");
    $this->assertSession()->statusCodeEquals(403);

    // Test access in another language.
    $de_nid = $node_de->id();
    $this->drupalGet("node/$de_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("node/$de_nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("fr/node/$de_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("fr/node/$de_nid/delete");
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet("de/node/$de_nid");
    $this->assertSession()->elementNotExists('xpath', '//a[text()=\'Delete\']/@href');
    $this->drupalGet("de/node/$de_nid/delete");
    $this->assertSession()->statusCodeEquals(403);
  }

}
